package bankaccount;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class GUI6 {
	private Control control;
	private JFrame frame;
	private JPanel panelMain;
	private JPanel panel;
	private JLabel title;
	private JLabel title1;
	private JTextField money;
	private JTextField money1;
	private JButton submit;
	private JButton submit1;
	private JLabel balance;
		
	public GUI6(){
		control = new Control();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(900, 140);
		
		panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		
		
		title = new JLabel(" deposit : ");
		title1 = new JLabel(" withdraw : ");
		
		money = new JTextField(9);
		money1 = new JTextField(8);
		
		submit = new JButton("submit");
		submit1 = new JButton("submit");
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
					control.deposit(money.getText());
					balance.setText("total :  " + control.getBalance() + "    baht");
				
			}
		});
		
		submit1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
					control.withdraw(money1.getText());
					balance.setText("total :  " + control.getBalance() + "    baht");
				
			}
		});
		
		
		balance = new JLabel("total :  0.0    baht");
		

		panel.add(title);
		panel.add(money);
		panel.add(submit);
		panel.add(title1);
		panel.add(money1);
		panel.add(submit1);
		panel.add(balance);
		panelMain.add(panel);
		frame.add(panelMain);
		frame.setVisible(true);
	}
}
