package bankaccount;



public class Control {
	private Bank bank;
	private int nwd;
	
	public Control(){
		bank = new Bank(); 
	}
	
	public void deposit(String p){
		bank.deposit(p);
	}
	
	public void withdraw(String p){
		double a = Double.parseDouble(p);
		double b = Double.parseDouble(bank.getBalance());

		if (b < a){
			nwd = 1;
		}
		else{
			nwd = 0;
			bank.withdraw(p);
		}
	}
	
	public String getBalance(){
		return bank.getBalance();
	}
	
	public int getValue(){
		return nwd;
	}
}
