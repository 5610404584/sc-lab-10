package bankaccount;



import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class GUI7 {
	private Control control;
	private JFrame frame;
	private JPanel panelMain;
	private JPanel panel;
	private JPanel panel2;
	private JLabel title;
	private JLabel title1;
	private JTextField money;
	private JTextField money1;
	private JButton submit;
	private JButton submit1;
	private JTextArea balance;
		
	public GUI7(){
		control = new Control();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(800, 500);
		
		panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		
		
		
		
		title = new JLabel(" Deposit : ");
		title1 = new JLabel(" Withdraw : ");
		
		money = new JTextField(9);
		money1 = new JTextField(8);
		
		submit = new JButton("submit");
		submit1 = new JButton("submit");
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
					control.deposit(money.getText());
					balance.append("Deposit :  " + money.getText() + "    bath\n");
					balance.append("total :  " + control.getBalance() + "    bath\n");

				
			
			}
			
		});
		
		submit1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

					control.withdraw(money1.getText());
										
						balance.append("Withdraw :  " + money1.getText() + "    bath\n");
						balance.append("total :  " + control.getBalance() + "    bath\n");
					
				}
			
		});
		
		
		balance = new JTextArea(8,15);
		

		panel.add(title);
		panel.add(money);
		panel.add(submit);
		panel.add(title1);
		panel.add(money1);
		panel.add(submit1);
		panel2.add(balance);
		panelMain.add(panel);
		frame.add(panelMain);
		frame.add(panel2, BorderLayout.SOUTH);
		frame.setVisible(true);
	}
}
