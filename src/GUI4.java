import javax.swing.JCheckBoxMenuItem;




import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUI4
{    
   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;
   
   private String[] distname = {"red","blue","green"} ;

   private JButton button;
   private JButton button1;
   private JButton button2;
   private JPanel panel;
   private JPanel panel1;
   private JComboBox r = new JComboBox(distname) ;

   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUI4();
   }
   
   public GUI4()
   {
	  createButton();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(300, 300);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      panel.add(r);

   }
   private void createButton()
   {
	
		   
		   class AddInterestListener implements ActionListener
		      {
			   
		         public void actionPerformed(ActionEvent event)
		         {
		        	 if(r.getSelectedItem() == "red"){
		        	 panel.setBackground(Color.RED);
		        	 panel1.setBackground(Color.RED);
		         }
		        	 else if(r.getSelectedItem() == "blue"){
		        		 panel.setBackground(Color.BLUE);
		            	 panel1.setBackground(Color.BLUE);
		        	 }
		        	 else if(r.getSelectedItem() == "green"){
		        		 panel.setBackground(Color.GREEN);
			        	 panel1.setBackground(Color.GREEN);
		        	 }
		      }}
		      
		      ActionListener red = new AddInterestListener();
		      r.addActionListener(red);

		   }
	  

   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}