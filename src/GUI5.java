import javax.swing.JCheckBoxMenuItem;







import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUI5
{    
   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;
   
   private String[] distname = {"red","blue","green"} ;

   private JButton button;
   private JButton button1;
   private JButton button2;
   private JPanel panel;
   private JPanel panel1;
   private JMenuBar menubar = new JMenuBar();
   private JMenu select = new JMenu("Menu");
   private JMenuItem d1 = new JMenuItem("red");
   private JMenuItem d2 = new JMenuItem("blue");
   private JMenuItem d3 = new JMenuItem("green");

   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUI5();
   }
   
   public GUI5()
   {
	  createButton();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(300, 300);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      panel.add(menubar);
      panel.add(select);
      panel.add(d1);
      panel.add(d2);
      panel.add(d3);

   }
   private void createButton()
   {
	
		   
		   class AddInterestListener implements ActionListener
		      {
			   
		         public void actionPerformed(ActionEvent event)
		         {
		        	 if(event.getSource() == d1){
		        	 panel.setBackground(Color.RED);
		        	 panel1.setBackground(Color.RED);
		         }
		        	 else if(event.getSource() == d2){
		        		 panel.setBackground(Color.BLUE);
		            	 panel1.setBackground(Color.BLUE);
		        	 }
		        	 else if(event.getSource() == d3){
		        		 panel.setBackground(Color.GREEN);
			        	 panel1.setBackground(Color.GREEN);
		        	 }
		      }}
		      
		      ActionListener red = new AddInterestListener();
		      d1.addActionListener(red);
		      d2.addActionListener(red);
		      d3.addActionListener(red);

		   }
	  

   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}