import javax.swing.JCheckBoxMenuItem;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUI3
{    
   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;

   private JButton button;
   private JButton button1;
   private JButton button2;
   private JPanel panel;
   private JPanel panel1;
   private JCheckBoxMenuItem r ;
   private JCheckBoxMenuItem b ;
   private JCheckBoxMenuItem g ;
   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUI3();
   }
   
   public GUI3()
   {
	  createButton();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(300, 300);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      panel.add(r);
      panel.add(b);
      panel.add(g);
   }
   private void createButton()
   {
	   r = new JCheckBoxMenuItem("red");
	   b = new JCheckBoxMenuItem("blue");
	   g = new JCheckBoxMenuItem("green");
      
      class AddInterestListener implements ActionListener
      {
    	  
         public void actionPerformed(ActionEvent event)
         {
        	 if(r.isSelected()){
        		 panel.setBackground(Color.RED);
            	 panel1.setBackground(Color.RED);
       	  }
        	 if(b.isSelected()){
        		 panel.setBackground(Color.blue);
            	 panel1.setBackground(Color.blue);
       	  }
        	 if(g.isSelected()){
        		 panel.setBackground(Color.green);
            	 panel1.setBackground(Color.green);
       	  }
        	 if(r.isSelected() && b.isSelected()){
        		 panel.setBackground(Color.MAGENTA);
            	 panel1.setBackground(Color.MAGENTA);
       	  }
        	 if(r.isSelected() && g.isSelected()){
        		 panel.setBackground(Color.YELLOW);
            	 panel1.setBackground(Color.YELLOW);
        	 }
        	 if(b.isSelected() && g.isSelected()){
        		 panel.setBackground(Color.cyan);
            	 panel1.setBackground(Color.cyan);
        	 }
        	 if(b.isSelected() && g.isSelected() && r.isSelected()){
        		 panel.setBackground(Color.white);
            	 panel1.setBackground(Color.white);
        	 }
        	 
         }            
      }
      
      ActionListener red = new AddInterestListener();
      r.addActionListener(red);
      b.addActionListener(red);
      g.addActionListener(red);

   }
  
   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}