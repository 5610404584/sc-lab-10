import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class GUI2
{    
   private static final int FRAME_WIDTH = 300;
   private static final int FRAME_HEIGHT = 300;

   private JButton button;
   private JButton button1;
   private JButton button2;
   private JPanel panel;
   private JPanel panel1;
   private JRadioButtonMenuItem r ;
   private JRadioButtonMenuItem b ;
   private JRadioButtonMenuItem g ;
   JFrame frame = new JFrame();
   
   public static void main(String[] args){
	   new GUI2();
   }
   
   public GUI2()
   {
	  createButton();
	  createButton1();
	  createButton2();
	  createPanel(); 
	  createPanel1(); 
	  frame.setLayout(new BorderLayout());
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.pack();
      frame.setLocationRelativeTo(null);
      frame.setVisible(true); 
      frame.setSize(300, 300);
      frame.add(panel1,BorderLayout.CENTER);
      frame.add(panel,BorderLayout.SOUTH);
      panel.add(r);
      panel.add(b);
      panel.add(g);
   }
   private void createButton()
   {
	   r = new JRadioButtonMenuItem("red");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.RED);
        	 panel1.setBackground(Color.RED);
         }            
      }
      
      ActionListener red = new AddInterestListener();
      r.addActionListener(red);

   }
   private void createButton1()
   {
	  b = new JRadioButtonMenuItem("blue");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.BLUE);
        	 panel1.setBackground(Color.BLUE);
         }            
      }
      
      ActionListener blue = new AddInterestListener();
      b.addActionListener(blue);
   }
   private void createButton2()
   {
	  g = new JRadioButtonMenuItem("green");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
        	 panel.setBackground(Color.GREEN);
        	 panel1.setBackground(Color.GREEN);
         }            
      }
      ActionListener green = new AddInterestListener();
      g.addActionListener(green);
   }

   private void createPanel()
   {
	   panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); 

   } 
   private void createPanel1()
   {
	   panel1 = new JPanel(); 

   } 

   
}